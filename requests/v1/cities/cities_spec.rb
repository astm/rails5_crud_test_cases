require 'rails_helper'

RSpec.describe 'Cities api', type: :request do
  before(:each) do
    # Create citys
    @city = create(:city)
    @city2 = create(:city)
    @city3 = create(:city)
    @city4 = create(:city)
    @city5 = create(:city)

    # Visit page
    get '/api/v1/cities'
  end

  context 'With valid data' do
    it 'JSON body response contains expected attributes' do
      result = JSON.parse(response.body)
      expect(result['cities'].length).to eq(5)
    end

    it 'Returns status code 200' do
      expect(response.code).to eq('200')
    end
  end
end
