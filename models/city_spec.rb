require 'rails_helper'

RSpec.describe City, type: :model do
  # Model relations
  describe 'Model relations' do
    it 'City has many shipment_consignees' do
      should have_many(:shipment_consignees)
    end

    it 'City has many areas' do
      should have_many(:areas)
    end

    it 'City has one country' do
      should belong_to(:country)
    end
  end

  # Require fields validation
  describe 'Require validations' do
    it 'name must be require' do
      should validate_presence_of(:name)
    end

    it 'name must be unique' do
      should validate_uniqueness_of(:name).case_insensitive
    end

    it 'prefix must be require' do
      should validate_presence_of(:prefix)
    end

    it 'prefix must be unique' do
      should validate_uniqueness_of(:prefix).case_insensitive
    end

    it 'longitude must be require' do
      should validate_presence_of(:longitude)
    end

    it 'latitude must be require' do
      should validate_presence_of(:latitude)
    end

    it 'state must be require' do
      should validate_presence_of(:state)
    end
  end

  # Test scopes
  describe 'Model Scopes' do
    before do
      # Create city
      @city = create(:city)
    end

    # Define params
    let(:params) { { state: 0, search: 'test name' } }

    it 'Test list by inactive state' do
      expect(City.state(params).count).to eql 1
    end

    it 'Test list by active state' do
      # change state
      @city.state = 1
      @city.save
      # change params value
      params.merge!(state: 1)
      # Expectation
      expect(City.state(params).count).to eql 1
    end

    it 'Test Search by name' do
      expect(City.search(params).count).to eql 1
    end

    it 'Test Search by prefix' do
      # change params value
      params.merge!(search: 'test prefix')
      expect(City.search(params).count).to eql 1
    end
  end
end
