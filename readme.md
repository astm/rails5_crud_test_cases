## Simple Rails 5 CRUD operations test cases

## Needed Gems
`````
source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.6.4'

#  Main Gems
gem 'rails', '~> 5.2.4'
gem 'pg', '>= 0.18', '< 2.0'
gem 'puma', '~> 3.11'
gem 'tzinfo-data'

# For images
gem 'paperclip', '~> 6.0.0'

# For Authentication
gem 'devise', '~> 4.6', '>= 4.6.1'
gem 'cancancan', '~> 2.3'
gem 'jwt'

# For Api's
gem 'jbuilder', '~> 2.5'
gem 'rest-client'

# For default data
gem 'faker', '~> 1.9', '>= 1.9.3'

group :development, :test do
  gem 'rspec-rails', '~> 3.8', '>= 3.8.2'
  gem 'factory_bot_rails'
  gem 'guard', '~> 2.15'
  gem 'guard-rspec', '~> 4.7', '>= 4.7.3'
end

group :test do
  gem 'capybara', '~> 3.14'
  gem 'selenium-webdriver'
  gem 'database_cleaner', '~> 1.7'
  gem 'shoulda-matchers', '~> 4.0', '>= 4.0.1'
  gem 'rails-controller-testing', '~> 1.0', '>= 1.0.4'
end

`````

### [Created by Astm Ali](https://github.com/astmdesign/)
