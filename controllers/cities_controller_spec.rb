require 'rails_helper'

RSpec.describe Admin::CitiesController, type: :controller do
  before(:each) do
    # Create admin
    @admin = create(:admin)
    sign_in @admin
    # Create city
    @city = create(:city)
  end

  # Testing index
  describe 'GET #index ' do
    it 'Listing the cities' do
      get :index
      expect(City.last).to eq(@city)
    end

    it 'Renders the :index view' do
      get :index
      expect(response).to render_template :index
    end
  end

  # Testing show
  describe 'GET #show ' do
    it 'assigns the requested city to @city' do
      get :show, params: { id: @city }
      assigns(:city).should eq(@city)
    end

    it 'Renders the #show view' do
      get :show, params: { id: @city }
      response.should render_template :show
    end
  end

  # Testing create
  describe 'POST create' do
    context 'with valid attributes' do
      it 'creates a new city' do
        expect {
          post :create, params: { city: FactoryBot.create(:city).attributes }
        }.to change(City, :count).by(1)
      end

      it 'redirects to the cities' do
        post :create, params: { city: attributes_for(:city) }
        # response.should redirect_to admin_cities_path
        # expect(response).to redirect_to(admin_cities_path)
        # response.should render_template :index
      end
    end

    context 'with invalid attributes' do
      it 'does not save the new city' do
        expect {
          post :create, params: { city: FactoryBot.attributes_for(:invalid_city) }
        }.to_not change(City, :count)
      end

      it 're-renders the new method' do
        post :create, params: { city: FactoryBot.attributes_for(:invalid_city) }
        response.should render_template :new
      end
    end
  end

  # Testing update
  describe 'PUT update' do
    context 'valid attributes' do
      it 'located the requested city' do
        put :update, params: { id: @city, city: FactoryBot.attributes_for(:city) }
        assigns(:city).should eq(@city)
      end

      it 'changes city attributes' do
        put :update, params: { id: @city, city: FactoryBot.attributes_for(:city, name: 'new name', prefix: 'new prefix', longitude: 'new longitude', latitude: 'new latitude') }
        @city.reload
        @city.name.should eq('new name')
        @city.prefix.should eq('new prefix')
      end

      it 'redirects to the updated city' do
        put :update, params: { id: @city, city: FactoryBot.attributes_for(:city) }
        response.should redirect_to admin_city_path(@city)
      end
    end

    context 'invalid attributes' do
      it 'locates the requested city' do
        put :update, params: { id: @city, city: FactoryBot.attributes_for(:invalid_city) }
        assigns(:city).should eq(@city)
      end

      it 'does not change city attributes' do
        put :update, params: { id: @city, city: FactoryBot.attributes_for(:city, name: 'invalid name', prefix: nil) }
        assigns(:city).should eq(@city)
      end

      it 're-renders the edit method' do
        put :update, params: { id: @city, city: FactoryBot.attributes_for(:invalid_city) }
        response.should render_template :edit
      end
    end
  end

  # Testing delete
  describe 'DELETE destroy' do
    it 'deletes the city' do
      expect {
        delete :destroy, params: { id: @city }
      }.to change(City, :count).by(-1)
    end

    it 'redirects to city#index' do
      delete :destroy, params: { id: @city }
      expect(@city).to redirect_to admin_cities_url
    end
  end
end
