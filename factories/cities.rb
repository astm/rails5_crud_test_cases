FactoryBot.define do
  factory :city do
    country
    sequence(:name) { |n| "test name_#{n}" }
    sequence(:prefix) { |n| "test prefix_#{n}" }
    longitude { 'test longitude' }
    latitude { 'test latitude' }
    state { 'inactive' } # the default in enum
  end

  factory :invalid_city, parent: :city do |f|
    f.name { nil }
    f.prefix { nil }
  end
end
