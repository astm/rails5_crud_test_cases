require 'rails_helper'

RSpec.feature 'Listing City' do
  before do
    # Create admin
    @admin = create(:admin)
    login_as(@admin, scope: :admin)

    # Create setting
    @setting = create(:setting)
    # Create city
    @city1 = create(:city)
    @city2 = create(:city)

    # Visit page
    visit admin_cities_path
  end

  scenario 'Should have list of cities' do
    expect(page).to have_content(@city1.name)
    expect(page).to have_content(@city2.name)
  end
end
