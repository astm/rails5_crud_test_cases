require 'rails_helper'

RSpec.feature 'Deleting City' do
  before do
    # Create admin
    @admin = create(:admin)
    login_as(@admin, scope: :admin)

    # Create setting
    @setting = create(:setting)
    # Create city
    @city = create(:city)

    # Visit page
    visit admin_cities_path
  end

  scenario 'Should have list of cities' do
    # Expectations
    expect(page).to have_content(@city.name)

    # Delete
    find('.delete').click

    # Expectations
    expect(page).to have_content('City was successfully destroyed.')
    expect(page).not_to have_content(@city.name)
  end
end
