require 'rails_helper'

RSpec.feature 'Editing City' do
  before do
    # Create admin
    @admin = create(:admin)
    login_as(@admin, scope: :admin)

    # Create setting
    @setting = create(:setting)
    # Create country
    @country = create(:country)
    # Create city
    @city = create(:city)

    # Visit page
    visit edit_admin_city_path(@city)
  end

  scenario 'With valid data' do
    # Fill form data
    select(@country.name, from: 'Country').select_option
    fill_in 'English Name', with: 'test name_0'
    fill_in 'Prefix', with: 'test prefix_0'
    fill_in 'longitude', with: 'test longitude'
    fill_in 'latitude', with: 'test latitude'
    select('Enabled', from: 'State').select_option
    click_button 'Save'

    # Redirect to listing page
    expect(page).to have_current_path(admin_city_path(@city))
  end

  scenario 'With invalid data' do
    # Fill form data
    fill_in 'English Name', with: nil
    fill_in 'Prefix', with: nil
    fill_in 'longitude', with: nil
    fill_in 'latitude', with: nil
    select('Enabled', from: 'State').select_option
    click_button 'Save'

    # Expectations
    expect(page).to have_content('Name can\'t be blank')
    expect(page).to have_content('Prefix can\'t be blank')
  end

  scenario 'With duplicated data' do
    # Create city
    @city2 = create(:city)

    # Fill form data
    fill_in 'English Name', with: @city2.name
    fill_in 'Prefix', with: @city2.prefix
    fill_in 'longitude', with: @city2.longitude
    fill_in 'latitude', with: @city2.latitude
    select('Enabled', from: 'State').select_option
    click_button 'Save'

    # Expectations
    expect(page).to have_content('Name has already been taken')
    expect(page).to have_content('Prefix has already been taken')
  end
end
